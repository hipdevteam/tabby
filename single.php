<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tabby
 */

get_header();
?>
	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<div class="single-post">
					<div class="row my-5">
						<?php echo ((is_active_sidebar('post_sidebar')) ? '<div class="col-12 col-md-7 col-lg-8">' : '<div class="col-12 col-md-12 col-lg-12">'); ?>
						<?php while ( have_posts() ) :the_post(); ?>
							<div class="post-content">
								<?php // if(has_post_thumbnail()):
									// the_post_thumbnail('770x385', array('class'=> 'img-fluid rounded', 'alt' => 'blog-img'));
								// endif; ?>
								<?php if(is_singular( 'post' )): ?>
								<h1><?php echo get_the_title($post->ID);?></h1>
								<ul class="post-meta my-3">
									<li class="list-inline-item"><?php _e('by ','tabby'); ?><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_author(); ?></a></li>
									<li class="list-inline-item"><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_time('F j, Y'); ?></a></li>
								</ul>
								<?php endif; ?>
								<div class="content">
									<?php the_content(); ?>
								</div>
								<?php if(is_singular( 'post' )): ?>
								<div class="wp-block-button tabby-block-btn primary small with-shadow mt-3" style="text-align: center;">
									<a class="wp-block-button__link" href="<?php echo get_post_type_archive_link( 'post'); ?>">Back to Blog</a>
								</div>
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
						</div>
						<?php if ( is_active_sidebar('post_sidebar')) : ?>
							<div class="col-12 col-md-5 col-lg-4">
								<div class="post-sidebar">
										<?php dynamic_sidebar('post_sidebar'); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
<?php
get_footer();
