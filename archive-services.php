<?php
get_header();
$count = 1;

$template = carbon_get_theme_option('tabby_services_archive_layout');
$conditionCount = 0;
query_posts(array(
	'post_type' => 'services',
	'posts_per_page' => -1,
	'orderby' => 'id',
	'order' => 'DESC'
));
?>
<?php if($template == 'layout-1'): ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="archive-conditions py-4">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="conditions-<?php echo $template?>">
								<?php
								if(have_posts()):
									while(have_posts()): the_post();
										$conditionCount++;
										?>
										<?php get_template_part('template-parts/services-'.$template);?>
										<?php
									endwhile;
								endif;
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
<?php elseif ($template == 'layout-2'):?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="archive-conditions py-4">
				<div class="container">
					<div class="row services-<?php echo $template?>">
						<?php
						if(have_posts()):
							while(have_posts()): the_post();
								$conditionCount++;
								?>
								<?php get_template_part('template-parts/services-'.$template);?>
								<?php
							endwhile;
						endif;
						?>
					</div>
				</div>
			</div>
		</main>
	</div>
<?php else: ?>
	<div class="tabby-service-archive-wrapper">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php if(have_posts()): while(have_posts()): the_post();?>
						<div class="tabby-service-post">
							<div class="row align-items-md-center <?php echo $count % 2 == 0 ? 'flex-md-row-reverse':''?>">
								<div class="col-12 col-sm-5 col-md-6 left">
									<?php if(has_post_thumbnail()): ?>
										<div class="tabby-service-image" style="background-image: url('<?php the_post_thumbnail_url('large');?>')"></div>
									<?php endif;?>
								</div>
								<div class="col-12 col-sm-7 col-md-6 right">
									<div class="tabby-service-content-wrapper">
										<div class="tabby-service-content">
											<h2 class="title"><?php the_title();?></h2>
											<p class="excerpt">
												<?php echo wp_trim_words(get_the_excerpt(get_the_ID()),40,'...'); ?>
											</p>
										</div>
										<div class="tabby-service-read-more">
											<a href="<?php the_permalink();?>" class="button-primary-outline small-size">Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $count++; endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>




<?php
if(carbon_get_theme_option('tabby_services_archive_testimonial') == 'yes'){
	echo do_shortcode('[hip_reviews]');
}
get_footer();