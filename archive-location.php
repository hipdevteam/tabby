<?php
/**
 * The template for displaying the location archive page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */

get_header();
?>
<style>
    .locations {
        display: none;
    }
</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
			$postType = get_queried_object();
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts(array(
				'post_type' =>'location',
				'paged' => $paged,
				'posts_per_page' => 12
			));
            $tabbyArchiveLayout = carbon_get_theme_option('tabby_default_archive_layout');
            // Want to get a count of remaing post in order to center the div class below rather than left align
            $count_posts = wp_count_posts('location');
            $published_posts = $count_posts->publish;
            $i = 0; // This variable is used to decrement the remaining published posts
            $newRowDivider = 4;

            // We also want to get a row count. We are going to check the remaining post every time we start a new row
            $count = 1;

            function getRowCount($count, $remaining_count, $rowDivider=4) {
                if($remaining_count >= 3) {
                    echo 4;
                    return 4;
                } elseif($count == 1) {
                    $newRowDivider = 12 / $remaining_count;
                    echo $newRowDivider;
                    return $newRowDivider;
                } else {
                    echo $rowDivider;
                    return $rowDivider;
                }
            }
            
		?>
			<div class="archive-staff">
				<div class="container">
					<div class="row my-3">
						<?php if (have_posts() ): while (have_posts() ):the_post(); ?>
                                <?php 
                                    $remaining_count = $published_posts - $i;
                                    $i++;
                                    $address = get_post_meta( get_the_ID(), 'location_street_address', true );
                                    $city = get_post_meta( get_the_ID(), 'location_city', true );
                                    $officeInfo = get_post_meta( get_the_ID(), 'location_office_info', true );
                                    $state = get_post_meta( get_the_ID(), 'location_state', true );
                                    $zipcode = get_post_meta( get_the_ID(), 'location_zipcode', true );
                                    $phone = get_post_meta( get_the_ID(), 'location_phone_number', true );
                                    $stateOption = get_post_meta( get_the_ID(), 'state_option', true );
                                    $googleURL = get_post_meta( get_the_ID(), 'location_place_url', true );
                                ?>
                            <div class="col-12 col-md-<?php $newRowDivider = getRowCount($count, $remaining_count, $newRowDivider); ?> my-3">
                                <div class="location-excerpt" style="text-align: center">
                                    <a href="<?php the_permalink(); ?>" class="no-underline">
                                    <p class="primary-txt"><i class="fas fa-map-marker-alt" style="font-size: 40px"></i></p>
                                    <h3 class="primary-txt">
                                        <?php echo $city . ' ' . $officeInfo; ?>
                                    </h3>
                                    <?php 
                                        echo ((!empty($address)) ? "<p class='footer-secondary-color'>$address<br>":"") .'
                                            '. ((!empty($zipcode)) ? "$city, $state $zipcode </p>" : "");
                                    ?>
                                    <?php if (!empty($phone)) : ?>
                                            <div class='archive_cta'>
                                                <a href='tel:<?php echo $phone; ?>' class='no-underline'>
                                                    <p class='primary-txt'><i class='fas fa-mobile-alt' style='font-size: 1em'></i>&nbsp;&nbsp;<?php echo $phone; ?></p>
                                                </a>
                                            </div>
                                    <?php else: ?>
                                        <div class='archive_cta'><br></div>
                                    <?php endif; ?>
                                    <?php if ( $hours = get_post_meta( get_the_ID(), 'location_hours', true ) ): ?>
                                        <div class="location-hours-operation">
                                            <p><?php echo nl2br($hours) ?></p>
                                        </div>
                                    <?php endif; ?>
                                    </a>
                                    <div class="wp-block-button tabby-block-btn primary small with-shadow mt-3" id="location-get-direction" style="text-align: center;">
                                        <a class="wp-block-button__link" href="<?php echo $googleURL; ?>" target="_blank">Get Directions</a>
                                    </div>   
                                </div>
							</div>
                        <?php 
                            ($count !== 3) ? $count++ : $count = 1; // Reset the count at start of each row
                            endwhile; endif; 
                        ?>
                    </div>
				</div>
			</div>
		</main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
