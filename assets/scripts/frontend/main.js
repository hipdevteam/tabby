(function($) {
  var toggle = $(".c-hamburger");
  var mobilemenu = $(".mobile-menu-wrap");

  toggleHandler(toggle);

  function toggleHandler(toggle) {
    toggle.click(function(e) {
      e.preventDefault();
      $(this).toggleClass("is-active");
      mobilemenu.slideToggle();
      mobilemenu.toggleClass("is-open");
      $("body").toggleClass("prevent-scroll");
    });

    if (mobilemenu.hasClass("alternate-menu-style")) {
      $(document).on(
        "click",
        ".alternate-menu-style .menu-item-has-children .dropdown-btn",
        function() {
          $(this)
            .parent()
            .addClass("active");
        }
      );
      $(
        ".alternate-menu-style .menu-item-has-children .sub-menu .nav-back-link"
      ).click(function() {
        $(this)
          .parent()
          .parent()
          .parent()
          .removeClass("active");
      });
    } else {
    }
  }

  /**
   * Determine the mobile operating system.
   * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
   *
   * @returns {String}
   */
  function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
      return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return "iOS";
    }

    return "unknown";
  }

  function toggleMenu(menu) {
    if (menu.parent().hasClass("selected")) {
      menu.parent().removeClass("selected");
    } else {
      menu.parent().addClass("selected");
    }

    menu
      .parent()
      .siblings(".tab.menu")
      .removeClass("selected");
  }

  // Tricks iOS tool bar to stay open at all times.
  if (getMobileOperatingSystem() == "iOS") {
    $("html").css("height", "100%");
    $("html").css("overflow-y", "scroll");
    $("html").css("-webkit-overflow-scrolling", "touch");

    $("body").css("height", "100%");
    $("body").css("overflow-y", "scroll");
    $("body").css("-webkit-overflow-scrolling", "touch");

    $(window).on("load", function() {
      var datePickerFields = $(".flatpickr-input");
      if (datePickerFields.length > 0) {
        datePickerFields.each(function() {
          $(this).on("focus", function() {
            $("html").css("overflow-y", "unset");
            $("body").css("overflow-y", "unset");
          });
          $(this).on("change", function() {
            $("html").css("overflow-y", "scroll");
            $("body").css("overflow-y", "scroll");
          });
        });
      }
    });
  }

  //alternate menu js
  $(".tabbar-wrapper.alternate-menu-style .tabbar-button").attr(
    "id",
    "alternate"
  );
  $(document).ready(function() {
    $(".menu")
      .parent()
      .addClass("main-menu-wrapper");
  });
  //$('.menu-item-has-children > a').removeAttr('href');
  var backLink =
    '<li class="menu-item menu-item-type-post_type"><a class="nav-link nav-back-link" href="#"><span class="fa fa-angle-left"> Back</a></li>';
  $(".alternate-menu-style ul > .menu-item-has-children .sub-menu").prepend(
    backLink
  );

  $(document).on(
    "click",
    ".alternate-menu-style .menu-item-has-children .dropdown-btn",
    function() {
      $(this)
        .parent()
        .addClass("active");
    }
  );

  $(
    ".alternate-menu-style .menu-item-has-children .sub-menu .nav-back-link"
  ).click(function() {
    $(this)
      .parent()
      .parent()
      .parent()
      .removeClass("active");
  });

  var toggleClass = $(
    ".alternate-menu-style .tabbar-button, .alternate-menu-style-btn button"
  );

  toggleClass.click(function() {
    document.body.classList.toggle("alternate-is-toggled");
  });
  // //default toggle class for menu
  // $('ul  li.menu-item-has-children .dropdown-btn').click(function () {
  // 	document.parent().classList.toggle('rotated');
  // })

  //Submenu Dropdown Toggle
  if ($("ul li.menu-item-has-children ul").length) {
    $("ul  li.menu-item-has-children").append(
      '<div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>'
    );

    //Dropdown Button
    $("ul  li.menu-item-has-children .dropdown-btn")
      .not(".alternate-menu-style ul li.menu-item-has-children .dropdown-btn")
      .on("click", function() {
        $(this)
          .prev(".sub-menu")
          .slideToggle(500);
        $(this)
          .parent()
          .toggleClass("rotated");
      });
  }

  $(".tab .sub-menu li:not(.menu-item-has-children) a").click(function(e) {
    e.stopPropagation();
  });

  $(".tab button").click(function() {
    if ($(this).data("url")) {
      $(this)
        .parent()
        .addClass("selected");
      location = $(this).data("url");
    } else {
      toggleMenu(
        $(this)
          .parent()
          .children()
      );
    }
  });

  // Add in the sticky header
  if (
    $("body").hasClass("tabby-sticky-header") &&
    document.querySelector(".header-main ul.menu")
  ) {
    var windowPositionFromTop = $(window).scrollTop();
    if (!!document.querySelector(".header-main ul.menu") == false) {
      console.log($("body.tabby-sticky-header #page").css("paddingTop"));
      $("body.tabby-sticky-header #page").css("paddingTop", "0px");
      console.log($("body.tabby-sticky-header #page").css("paddingTop"));
    }
    if (
      windowPositionFromTop > 100 &&
      !!document.querySelector(".header-main ul.menu")
    ) {
      $("body").addClass("header-is-sticky");
      submenuTopPaddingSet();
    }

    $(window).on("scroll", function() {
      var windowPositionFromTop = $(window).scrollTop();
      if (
        windowPositionFromTop > 100 &&
        !!document.querySelector(".header-main ul.menu")
      ) {
        $("body").addClass("header-is-sticky");
      } else {
        $("body").removeClass("header-is-sticky");
      }
      setTimeout(function() {
        submenuTopPaddingSet();
      }, 2000);
    });
  }

  /** Adjust the Main nav subMenu top padding automatically **/
  $(document).ready(function() {
    if (!!document.querySelector(".header-main ul.menu")) {
      submenuTopPaddingSet();
    }
  });

  function submenuTopPaddingSet() {
    var mainNav = document.querySelector(".header-main ul.menu");
    var mainNavHeight = mainNav.offsetHeight;
    var mainHeader = document.querySelector("header.header .header-main");
    var mainHeaderHeight = mainHeader.offsetHeight;
    var subMenus = $("#menu-primary-menu.menu > li ul").not(
      "#menu-primary-menu.menu > li ul li ul"
    );

    var offsetPadding = (mainHeaderHeight - mainNavHeight) / 2;

    subMenus.each(function(subMenu) {
      subMenu.style.paddingTop = offsetPadding + "px";
    });
  }

  /** owlcarousel settings **/
  if (jQuery().owlCarousel) {
    $(".hip-info-box-carousel").owlCarousel({
      loop: true,
      margin: 0,
      nav: true,
      navText: [
        "<i class='fas fa-long-arrow-alt-left'></i>",
        "<i class='fas fa-long-arrow-alt-right'></i>"
      ],
      dots: true,
      slideBy: 1,
      dotsEach: true,
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 2
        },
        1100: {
          items: 3
        }
      }
    });
  }
})(jQuery);
