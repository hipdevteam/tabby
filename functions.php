<?php

/**
 * Initialize theme settings
 */
new \Tabby\ThemeOptions();

/**
 * Initialize custom Gutenberg blocks
 */
add_action('after_setup_theme', function () {
	new \Tabby\GutenbergBlocks();
});

/**
 * tabby functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tabby
 */

/*
 * Theme Version
 *
 * Added as query strings to stylesheets and scripts to bust  browser cache.
 */

define('THEME_VERSION', '1.8.3');

if (! function_exists('tabby_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tabby_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tabby, use a find and replace
		 * to change 'tabby' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('tabby', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus([
			'main'		  =>  esc_html__('Primary menu', 'tabby'),
			'secondary'	  =>  esc_html__('Secondary header menu', 'tabby'),
			'footer'	  => esc_html__('Footer Menu', 'tabby'),
			'mobile'	  => esc_html__('Mobile Dropdown Menu', 'tabby'),
			'hero'		  => esc_html__('Hero Dropdown Menu', 'tabby')
		]);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('tabby_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support('custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		));
		/**
		 * can be moved into separate function or file
		 */
		add_shortcode('tabby-breadcrumbs', function ($atts) {
			global $post;
			if ($post) {
				$breadcrumbs = new \Tabby\Breadcrumbs($post);
				return $breadcrumbs->render();
			}
			return false;
		});

		/**
		 * add image size tabby theme
		 */
		add_image_size('100x65', 100, 65);
		add_image_size('770x385', 770, 385, true);
		add_image_size('737x478', 737, 478, true);
	}
endif;
add_action('after_setup_theme', 'tabby_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tabby_content_width()
{
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters('tabby_content_width', 640);
}
add_action('after_setup_theme', 'tabby_content_width', 0);

/**
 * Enqueue scripts and styles.
 */
function tabby_scripts()
{
	/**
	 * Styles
	 */
	if (!is_plugin_active('hip-reviews/plugin.php')) {
		wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/dist/lib/owl-carousel/dist/assets/owl.carousel.min.css');
	}
	
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/dist/lib/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('tabby-main', get_template_directory_uri() . '/dist/css/combined.css');
	wp_enqueue_style('tabby-font-awesome-5', get_template_directory_uri() . '/dist/vendor/fontawesome/css/all.css');

	$uploads = wp_upload_dir();
	$theme_styles = $uploads['basedir'] . '/tabby.css';
	
	if ( ! file_exists($theme_styles) ) {
		cacheThemeStyles();
	}
	

	if ( file_exists($theme_styles) ) {
		$vers = get_option('theme_options_version', 'unknown');
		$url = $uploads['baseurl'] . '/tabby.css';
		wp_enqueue_style('tabby-theme-styles', $url, [], $vers);
	}


	/**
	 * Scripts
	 */
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/dist/lib/bootstrap/js/bootstrap.min.js', ['jquery'], '4.3.1', true);
	wp_enqueue_script('tabby-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);
	wp_enqueue_script('tabby-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
	if (!is_plugin_active('hip-reviews/plugin.php')) {
		wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/dist/lib/owl-carousel/dist/owl.carousel.min.js', ['jquery'], '', true);
	}
	wp_enqueue_script('tabby-main', get_template_directory_uri() . '/dist/js/combined.js', ['jquery'], THEME_VERSION, true);
}
add_action('wp_enqueue_scripts', 'tabby_scripts');
function tabbyAdminScripts()
{
	wp_enqueue_style('tabby-admin-main', get_template_directory_uri().'/dist/css/admin.css', '', THEME_VERSION);
	wp_enqueue_script('tabby-admin-main', get_template_directory_uri() . '/dist/js/combined-admin.js', ['jquery'], THEME_VERSION, true);
	wp_localize_script('tabby-admin-main', 'ajaxOptions', [
		'ajaxUrl' => admin_url('admin-ajax.php')
	]);
}
add_action('admin_enqueue_scripts', 'tabbyAdminScripts');

/**
 * include theme scripts
 */
function includeFiles()
{
	/**
	 * Implement the Custom Header feature.
	 */
	require get_template_directory() . '/inc/custom-header.php';

	/**
	 * Custom template tags for this theme.
	 */
	require get_template_directory() . '/inc/template-tags.php';

	/**
	 * Functions which enhance the theme by hooking into WordPress.
	 */
	require get_template_directory() . '/inc/template-functions.php';

	/**
	 * Customizer additions.
	 */
	require get_template_directory() . '/inc/customizer.php';
	/**
	 * widget areas
	 */
	include get_template_directory() . '/inc/widgetAreas.php';
	/**
	 * Recent Post widget
	 */
	include get_template_directory() . '/inc/widgets/recent-post.php';
	/**
	 * Recent Post widget
	 */
	include get_template_directory() . '/inc/widgets/popular-categories.php';
}

add_action('after_setup_theme', 'includeFiles');

// Create a static file with all the styles generated from theme options only
// when the theme options are saved.
add_action('carbon_fields_theme_options_container_saved', 'cacheThemeStyles');

function cacheThemeStyles() {
	// Refresh the global variable holding all theme options
	globalCustomFields();

	// Set version string for cache busting
	update_option('theme_options_version', time());

	$templates = glob(get_template_directory().'/inc/css/*.css.php');

	ob_start();

	foreach ($templates as $template) {
		include($template);
	}

	$css = ob_get_contents();
	ob_end_clean();

	$uploads = wp_upload_dir();
	$filename = $uploads['basedir'] . '/tabby.css';
	file_put_contents( $filename, $css );
}

// Apply filter
add_filter('body_class', function ($classes) {
	global $tabbyFields;
	$isSticky = $tabbyFields['tabby_sticky_header'];
	$headerLayout = $tabbyFields['tabby_header_layout'];
	$mobileMenu = $tabbyFields['tabby_menu_layout'];
	$mobileMenuClass = 'tabby-hamburger-menu';
	if ($isSticky == 'yes' &&  ! is_page_template('landing-page.php')) {
		$classes[] = 'tabby-sticky-header';
	}
	if ($mobileMenu == 'tabbar_top') {
		$mobileMenuClass = 'tabby-tabbar-top';
	}
	if ($mobileMenu == 'tabbar_bottom') {
		$mobileMenuClass = 'tabby-tabbar-bottom';
	}

	if ( ! is_page_template('landing-page.php') ) {
		$classes[] = 'tabby-header-'.$headerLayout;
	}

	$classes[] = $mobileMenuClass;
	return $classes;
});

add_action('admin_head', function () {
	ob_start();
	?>
	<style id="tabby-admin-css" type="text/css">
		body .wp-block{
			max-width: 100%;
		}
		body .block-editor-block-list__layout .block-editor-default-block-appender > .block-editor-default-block-appender__content:empty{
			margin-top:5px;
			margin-bottom: 5px;
		}
		body .ugb-container{
			padding: 35px;
		}
	</style>
	<?php
	return ob_get_flush();
});

add_action('wp_ajax_get_post_type_terms', 'get_post_type_terms');
function get_post_type_terms()
{
	$postType =  $_POST['postType'];
	$taxonomies = get_taxonomies(array(
		'object_type' => [$postType],
	), 'ids', 'and');
	$taxonomy = reset($taxonomies);
	if ($taxonomy != null) {
		$terms   = get_terms(array(
			'taxonomy'   => $taxonomy->name,
			'hide_empty' => false,
		));
		if (!empty($terms)) {
			$allTerms = [];
			foreach ($terms as $term) {
				$allTerms[] = [
					'id' => $term->term_id,
					'name' => $term->name
				];
			}
			if (isset($_POST['withSelected'])) {
				$selected = get_option('hip_post_block_category');
				echo json_encode([
					'status' => 'has_terms',
					'taxonomy_slug'=>$taxonomy->name,
					'taxonomy_label'=>$taxonomy->label,
					'terms' => $allTerms,
					'selected'=> $selected
				]);
			} else {
				echo json_encode([
					'status' => 'has_terms',
					'taxonomy_slug'=>$taxonomy->name,
					'taxonomy_label'=>$taxonomy->label,
					'terms' => $allTerms,
				]);
			}
		}
	} else {
		echo json_encode([
			'status' => 'no_terms',
			'terms' => [],
		]);
	}
	wp_die();
}
add_action('wp_ajax_save_post_block_category', 'save_post_block_category');
function save_post_block_category()
{
	$categoryId =  $_POST['categoryId'];
	$updated = update_option('hip_post_block_category', $categoryId);
	if ($updated) {
		echo json_encode([
			'status' => 'updated successfully',
		]);
	}
	wp_die();
}

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**all custom fields global values**/
add_action('init', 'globalCustomFields');
function globalCustomFields()
{
	global $tabbyFields;
	global $wpdb;
	$tabbyFieldsResults = $wpdb->get_results("SELECT * FROM ". $wpdb->options ." WHERE option_name LIKE '_tabby%'");
	$tabbyFields = [];

	foreach ( $tabbyFieldsResults as $result ) {
		$new_name = ltrim($result->option_name, "_");
		$tabbyFields[$new_name] = $result->option_value;
	}
}

// Add excerpts to pages
add_post_type_support( 'page', 'excerpt' );

//Copyright Year shortcode

add_shortcode('hip_year', function() { return date('Y'); });
