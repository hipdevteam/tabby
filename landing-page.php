<?php
/**
 * Template Name: Landing Pages
 * Template Post Type: lp,page
 * @package tabby
 * @since 1.0.0
 */
get_header();
get_template_part('template-parts/banner');
?>
<div class="full-width-page-template">
	<?php
		if(have_posts()){
			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
		}
		wp_reset_postdata();
	?>
</div>
<?php get_footer();?>