<?php
/**
 * Template part for displaying page Breadcrumb Layout 3 *
 * @package tabby
 * @since 1.0.0
 */
?>
<div class="tabby-breadcrumb text-center text-md-left">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php echo do_shortcode('[tabby-breadcrumbs]');?>
			</div>
		</div>
	</div>
</div>