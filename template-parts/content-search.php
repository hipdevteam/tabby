<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */

?>

<article id="post-<?php the_ID(); ?>" class="search-results">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php echo wp_trim_words( get_the_excerpt(), 15, '...' ) ?>
	</div><!-- .entry-summary -->
</article><!-- #post-<?php the_ID(); ?> -->
