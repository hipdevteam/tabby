<?php
namespace Tabby\ThemeOptions;

use Carbon_Fields\Field;

class HeaderSettings
{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
	}
	public function renderSettingsFields()
	{
		return array_merge($this->headerSettings(), $this->headerHeightSettings(), $this->headerMenuDesktopSettings(),$this->hamburgerMobileMenu(), $this->archiveBannerSettings());
	}
	public function headerSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_top_section', __('Header Top style')),
			Field::make('color', 'tabby_header_top_bg', __('Header Top Background')),
			Field::make('color', 'tabby_header_top_link_color', __('Header Link Color'))
				->set_width(50),
			Field::make('color', 'tabby_header_top_link_hover', __('Header Link Hover'))
				->set_width(50),
			Field::make('separator', 'tabby_header_main_menu', __('Main Menu style')),
			Field::make('color', 'tabby_header_menu_color', __(' Menu color'))
				->set_width(50),
			Field::make('color', 'tabby_header_menu_color_hover', __(' Menu Hover Color'))
				->set_width(50),
			Field::make('separator', 'tabby_header_cta_btn_title', __('Header Cta button style')),
			Field::make('select', 'tabby_header_cta_btn_style', __('Button style'))
				->set_options(array(
					'rectangle' => 'Rectangle',
					'oval' => 'Oval'
				)),
			Field::make('color', 'tabby_header_cta_btn_bg_color', __('Button Background'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_hover_color', __('Button Hover Background'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_text_color', __('Button Text Color'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_text_hover_color', __('Button Text Hover Color'))->set_width(50),
			Field::make('select', 'tabby_header_cta_btn_border_bottom', __('Button border '))
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					)
				))
				->set_options(array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_header_cta_btn_border_btn_color', __('Border bottom Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_header_cta_btn_border_btn_hover_color', __('Border bottom Hover Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_header_cta_btn_border_btn_height', __('Border bottom Height'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

//			outside border style
			Field::make('select', 'tabby_header_cta_btn_border_outside', __('Outside border'))
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					)
				))
				->set_options(array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_header_cta_btn_border_outside_btn_color', __('Border  Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_header_cta_btn_border_outside_btn_hover_color', __('Border  Hover Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_header_cta_btn_border_outside_width', __('Border  Height'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
		);
	}
	public function headerMenuDesktopSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_main_bg_section', __('Header Sub Menu Styles For Desktop')),
			Field::make('color', 'tabby_header_main_desktop_bg', __('Background'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_bg_hover', __('Background Hover'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_text_color', __('Text Color'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_text_hover', __('Text Color Hover'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_icon_color', __('Icon Color'))->set_width(50),

		);
	}
	public function headerHeightSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_height_section', __('Header Height Settings for Desktop')),
			Field::make('text', 'tabby_header_height_top', __('Top Header Height')),
			Field::make('text', 'tabby_header_height_main', __('Main Header Height')),
			Field::make('text', 'tabby_header_height_page_header', __('Page Header Height')),
			Field::make('separator', 'tabby_header_height_mobile', __('Header Height Settings for Mobile')),
			Field::make('text', 'tabby_header_height_main_mobile', __('Main Header Height')),
			Field::make('text', 'tabby_header_height_page_header_mobile', __('Page Header Height')),

		);
	}
	public function hamburgerMobileMenu(){
		return [
			Field::make('separator', 'tabby_hamburger_section', __('Hamburger Menu Style')),
			Field::make('color', 'tabby_hamburger_icon_color', __('Hamburger Icon color'))
				->set_width(50),
			Field::make('color', 'tabby_hamburger_color', __('Menu color'))
				->set_width(50),
			Field::make('color', 'tabby_hamburger_hover_color', __('Menu Hover color'))->set_width(50),
			Field::make('color', 'tabby_hamburger_menu_icon_color', __('Menu Item Dropdown Icon Color'))->set_width(50),
			Field::make('color', 'tabby_hamburger_menu_submenu_color', __('Menu Submenu Color'))->set_width(50),
			Field::make('color', 'tabby_hamburger_menu_submenu_hover_color', __('Menu Submenu Hover Color'))->set_width(50),
			Field::make('color', 'tabby_hamburger_menu_border_color', __('Menu Item Border'))->set_width(50),
			Field::make('text', 'tabby_hamburger_menu_border_height', __('Menu Item Border Height'))->set_width(50),
			Field::make('text', 'tabby_hamburger_font_size', __('Menu Item Font Size'))->set_width(50),
			Field::make('text', 'tabby_hamburger_submenu_font_size', __('Menu Item Sub Menu Font Size'))->set_width(50),


			Field::make('select', 'tabby_hamburger_menu_width', __('Hamburger Menu Width'))
				->set_options(array(
					'container-width' => 'Container Width',
					'full-width' => 'Full Width'
				)),
			Field::make('color', 'tabby_hamburger_wrapper_bg', __('Menu Wrapper Background'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'container-width',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_hamburger_menu_item_bg', __('Menu Item Background'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'full-width',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_hamburger_menu_item_bg_hover', __('Menu Item Background Hover'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'full-width',
						'compare' => '=',
					)
				)),
			Field::make('color', 'tabby_hamburger_submenu_item_bg', __('Sub Menu Item Background'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'full-width',
						'compare' => '=',
					)
				)),
			Field::make('color', 'tabby_hamburger_submenu_item_bg_hover', __('Sub Menu Item Hover Background'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'full-width',
						'compare' => '=',
					)
				)),
			Field::make('color', 'tabby_hamburger_wrapper_bg_full_width', __('Hamburger Menu Wrapper Background'))
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_hamburger_menu_width',
						'value' => 'full-width',
						'compare' => '=',
					)
				)),


		];
	}
	public function archiveBannerSettings()
	{
		return [
			Field::make('separator', 'tabby_archive_banner_section', __('Archive Banner Style')),
			Field::make('image', 'tabby_archive_banner_image', __('Banner Image')) ->set_width(50),
			Field::make('color', 'tabby_archive_banner_overlay', __('Banner overlay color'))
				->set_width(50),
			Field::make('text', 'tabby_archive_banner_opacity', __('Banner opacity'))
				->set_width(50)->set_help_text('Add opacity Ex:0.8 dont add px'),
		];
	}
}
