<?php
namespace Tabby\ThemeOptions;
use Carbon_Fields\Field;

class FooterSettings{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
	}
	public function renderSettingsFields()
	{
		return array_merge($this->locationFooterSettings(), $this->aboveFooterSettings(), $this->mainFooterSettings(), $this->subFooterSettings());
	}
	public function locationFooterSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_location_footer_section', __( 'Locations in Footer' )),
			Field::make( 'radio', 'tabby_location_footer_style', __( 'Choose a Style'))
				->set_options( array(
					'no_locations' => 'Do not show locations',
					'interactive_map' => 'Show locations with interactive map',
					'without_interactive_map' => 'Show locations without interactive map'
				))
		);
	}
	public function aboveFooterSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_above_footer_section', __( 'Above Footer' )),
			Field::make( 'image', 'tabby_above_footer_bg', __( 'Background Image' )),
			Field::make( 'text', 'tabby_above_footer_bg_opacity', __( 'Background Image Opacity' ))
				->set_attribute('maxLength','3')
				->set_attribute('pattern','^[0-9][0-9]?$|^100$')
				->set_help_text( 'Please enter value between 0 to 100'),
			Field::make( 'color', 'tabby_above_footer_bg_color', __( 'Background Color' )),
			Field::make('text', 'tabby_above_footer_section_height', __('Section Height'))
				->set_width(50)
				->set_help_text('Here put your section height EX:300'),
			Field::make('separator', 'tabby_above_footer_email_field', __('Email Field Style')),
			Field::make('color', 'tabby_above_footer_email_bg', __('Email Background'))->set_width(50),
			Field::make('color', 'tabby_above_footer_email_text', __('Text Color'))->set_width(50),
			Field::make('color', 'tabby_above_footer_email_placeholder', __('Placeholder  Color'))->set_width(50),
			Field::make('separator', 'tabby_above_footer_button_styles', __('Submit Button Style')),
			Field::make('color', 'tabby_above_footer_btn_bg', __('Button Background'))->set_width(50),
			Field::make('color', 'tabby_above_footer_btn_hover', __('Button Background Hover'))->set_width(50),
			Field::make('color', 'tabby_above_footer_btn_text_color', __('Button Text color'))->set_width(50),
			Field::make('color', 'tabby_above_footer_btn_text_hover', __('Button Text Hover'))->set_width(50),
			Field::make('separator', 'tabby_above_form_submit', __('Submit Message')),
			Field::make('color', 'tabby_above_footer_submit_error_message_color', __('Submit Error Message Color'))->set_width(50),
			Field::make('color', 'tabby_above_footer_submit_success_message_color', __('Submit Success Message Color'))->set_width(50),
			Field::make('color', 'tabby_above_footer_submit_success_message_bg', __('Submit Success Message Background Color'))->set_width(50),
			Field::make('color', 'tabby_above_footer_submit_success_message_border_color', __('Submit Success Message Boder Color'))->set_width(50),

		);
	}
	public function mainFooterSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_main_footer_section', __( 'Main Footer' )),
			Field::make( 'color', 'tabby_main_footer_bg_color', __( 'Background Color' )),
			Field::make('color', 'tabby_main_footer_heading_color', __('Heading Color'))->set_width(50),
			Field::make('color', 'tabby_main_footer_text_color', __('Text Color'))->set_width(50),
			Field::make('color', 'tabby_main_footer_link_color', __('Link Color'))->set_width(50),
			Field::make('color', 'tabby_main_footer_link_hover_color', __('Link Hover Color'))->set_width(50),
		);
	}
	public function subFooterSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_sub_footer_section', __( 'Sub Footer' )),
			Field::make( 'color', 'tabby_sub_footer_bg_color', __( 'Background Color' ))->set_width(50),
			Field::make('color', 'tabby_sub_footer_text_color', __('Text Color'))->set_width(50),
			Field::make('color', 'tabby_sub_footer_link_color', __('Link Color'))->set_width(50),
			Field::make('color', 'tabby_sub_footer_link_hover_color', __('Link Hover Color'))->set_width(50),
		);
	}
}