<?php
namespace Tabby\ThemeOptions;
use Carbon_Fields\Field;

class BusinessSettings
{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
		add_shortcode('tabby-business-address',[$this,'renderAddress']);
		add_shortcode('tabby-business-phone-number',[$this,'renderPhoneNumberBasedOnCallrail']);
		add_shortcode('tabby-business-phone-number-for-links',[$this,'renderPhoneNumberForLinks']);
		add_shortcode('tabby-business-phone-number-link',[$this,'renderPhoneNumberLink']);
		add_shortcode('tabby-business-social-links',[$this,'renderSocialMediaLinks']);
	}
	public function renderSettingsFields()
	{
		return array_merge($this->businessInfoSettings(), $this->socialMediaSettings(), $this->socialMediaStyleSettings());
	}
	public function businessInfoSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_business_section', __( 'Business Info' )),
			Field::make( 'text', 'tabby_business_phone_number', __( 'Phone Number' ))->set_width( 50 ),
			Field::make( 'radio', 'tabby_callrail_avoidance', __( 'Keep Callrail from changing numbers?' ))
				->set_options( array(
						'1' => 'Yes',
						'0' => 'No'
					))->set_default_value('0'),
			Field::make( 'textarea', 'tabby_business_address', __( 'Business Info Address' ))->set_width( 50 )
		);
	}
	public function socialMediaSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_social_media_section', __( 'Business Info Social Media' )),
			Field::make( 'complex', 'tabby_social_meida', 'Add Social Media Icon and Link' )
				->add_fields( array(
					Field::make( 'text', 'icon', 'Icon Class' ),
					Field::make( 'text', 'link', 'Social Link' ),
				) ),

		);
	}
	public function socialMediaStyleSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_social_media_styles_section', __( 'Business Info Social Media Styles' )),
			Field::make( 'text', 'tabby_social_icon_height', __( 'Height' ))
				->set_help_text( 'Please enter value with unit. e.g. 20px or 1rem')
				->set_width( 33 ),
			Field::make( 'text', 'tabby_social_icon_width', __( 'Width' ))
				->set_help_text( 'Please enter value with unit. e.g. 20px or 1rem')
				->set_width( 33 ),
			Field::make( 'text', 'tabby_social_icon_font_size', __( 'Icon Font Size' ))
				->set_help_text( 'Please enter value with unit. e.g. 16px or 1rem')
				->set_width( 33 ),
			Field::make( 'color', 'tabby_social_icon_color', __( 'Icon Color' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_hover_color', __( 'Icon Hover Color' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_bg_color', __( 'Icon Background Color' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_bg_hover_color', __( 'Icon Background Hover Color' ))->set_width( 50 ),
		);
	}
	public function renderAddress()
	{
		ob_start();
		global $tabbyFields;
		$address = $tabbyFields['tabby_business_address'];
		?>
		<div class="tabby-business-address">
			<?php if(!empty($address)): ?>
			<p><?php echo nl2br($address);?></p>
			<?php else: ?>
			<p>Your address</p>
			<?php endif;?>
		</div>
		<?php
		return ob_get_clean();
	}
	public function renderPhoneNumberForLinks()
	{   global $tabbyFields;
		$phoneNumber = $tabbyFields['tabby_business_phone_number'];
		if(!empty($phoneNumber))
			return $phoneNumber;
		return 'Your phone number';
	}
	public function renderPhoneNumberBasedOnCallrail()
	{	
		global $tabbyFields;
		$phoneNumber = $tabbyFields['tabby_business_phone_number'];
		$phoneNumber = ($tabbyFields['tabby_callrail_avoidance'] == 1) ? implode('<span></span>', str_split($phoneNumber)) : $phoneNumber;
		if(!empty($phoneNumber))
			return $phoneNumber;
	}
	public function renderPhoneNumberLink()
	{
		ob_start();
		$phoneNumberForLinks = $this->renderPhoneNumberForLinks();
		$phoneNumber = $this->renderPhoneNumberBasedOnCallrail();
		?>
		<a href="tel:<?php echo $phoneNumberForLinks; ?>" class="tabby-business-phone-link">
				<?php echo $phoneNumber ?>
		</a>
		<?php
		return ob_get_clean();
	}
	public function renderSocialMediaLinks()
	{
		ob_start();
		global $tabbyFields;
		// Workaround for complex Carbon Field. Allows us to do something with each social media link and icon
		foreach($tabbyFields as $key => $value){
			// Carbon Field complex options are placed in array like _tabby_social_media|icon|0|etc. This helps us make sure the first section matches what we want
			$exp_key = explode('|', $key);
			if($exp_key[0] == 'tabby_social_meida'){
				// Create a new array with link or icon and then the number connected with it. This allows us to grab the link and icon together for each social media group
				$socialMediaLinks[$exp_key[1].$exp_key[2]] = $value;
			}
		}
		$i = 0;
		if(!empty($socialMediaLinks)):
		?>
		<ul class="tabby-social-links">
			<?php while(  $i < count($socialMediaLinks) ): ?>
			<?php if ( isset($socialMediaLinks['link'.$i]) ) : ?>
				<li>
					<a href="<?php echo $socialMediaLinks['link'.$i];?>" target="_blank"><i class="<?php echo $socialMediaLinks['icon'.$i]; ?>"></i></a>
				</li>
			<?php endif; ?>
		<?php $i++;endwhile; ?>
		</ul>
		<?php else: ?>
		<p>Your social links</p>
		<?php
		endif;
		return ob_get_clean();
	}

}