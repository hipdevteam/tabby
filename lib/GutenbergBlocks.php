<?php
namespace Tabby;
use Carbon_Fields\Block;
use Carbon_Fields\Field;
use League\Flysystem\Exception;

class GutenbergBlocks{
	private $googleApiKey = 'AIzaSyC1lJQcAk0_O8gigUc43fV4HaQpXCf9lUk';
	public function __construct()
	{
		$this->initBlocks();
	}
	public function initBlocks()
	{
		$this->staffBlock();
		$this->InfoCarousel();
		$this->videoEmbed();
		$this->hipPostsBlock();
	}
	public function staffCatList(){
		$is_admin = is_admin();
		$categories = ($is_admin) ? get_terms( 'staff_category' ): array('all' => 'All');
		$all_cat = array("all" => "All");
		if ( $categories ){
			foreach ( $categories as $cat )
				if(isset($cat->term_id)){
					$all_cat[$cat->term_id] = esc_html( $cat->name );
				}

		}
		return $all_cat;
	}
	public function getPostTypes()
	{
		$postTypes = get_post_types(['public'=>true],'objects');
		$allPostTypes = [];
		foreach ($postTypes as $postType){
			if($postType->name == 'fl-builder-template' ||$postType->name == 'attachment'){
				continue;
			}
			$allPostTypes[$postType->name] = $postType->label;
		}
		return $allPostTypes;
	}
	public function getVideoCollectionPosts()
	{
		$videos = get_posts([
			'post_type'=>'hip_video_collection',
			'posts_per_page'=> -1
		]);
		if(empty($videos)){
			return [0=>'You have no videos in collection'];
		}
		$allVideos = [];
		foreach ($videos as $video){
			$allVideos[$video->ID] = $video->post_title;
		}
		return $allVideos;
	}
	public function staffBlock()
	{
		Block::make( __( 'Hip Staff Block' ))
			->add_fields( array(
				Field::make( 'separator', 'hip_staff_block_title', __( 'Staff Members' )),
				Field::make( 'select', 'hip_staff_block_layout', __( 'Layout' ) )
					->set_options( array(
						'grid' => __('Grid','tabby'),
						'list' => __('List')
					)),

				Field::make( 'text', 'hip_staff_block_count', __( 'Number of posts' ))
					->set_attribute('pattern','^[0-9][0-9]?$|^100$')
					->set_help_text( 'Leave empty for showing all'),

				Field::make( 'select', 'hip_staff_block_cat', __( 'Category' ))
					->add_options( array( $this, 'staffCatList' ) )
					->set_help_text( 'Select category'),

				Field::make( 'select', 'hip_staff_block_order', __( 'Order' ))
					->set_options( array(
						'asc' => __('ASC','tabby'),
						'desc' => __('DESC')
					))
					->set_help_text( 'Ordering on post date')
			))
			->set_category( 'hip-blocks', __( 'Hip Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				$posts_per_page = $fields['hip_staff_block_count'];
				if(empty($posts_per_page) && !is_numeric($posts_per_page)){
					$posts_per_page = 6;
				}
				$staffCats = null;
				if(!empty($fields['hip_staff_block_cat'])):
					$staffCats = $fields['hip_staff_block_cat'];
				endif;
				if(!empty($staffCats)):
					$staffs = get_posts([
						'post_type'=> 'staff',
						'posts_per_page' => $posts_per_page,
						'orderby' => 'id',
						'order' => $fields['hip_staff_block_order'],
						'tax_query' => array(
							array(
								'taxonomy'  => 'staff_category',
								'terms'     => array( $staffCats),
								'field'     => 'term_id'
							)
						)
					]);
				else:
					$staffs = get_posts([
						'post_type'=> 'staff',
						'posts_per_page' => $posts_per_page,
						'orderby' => 'id',
						'order' => $fields['hip_staff_block_order']
					]);
				endif;
				ob_start();
				?>
				<?php
				if(!empty($fields['hip_staff_block_layout'])){
					if($fields['hip_staff_block_layout'] == 'grid'){
						include(get_template_directory().'/template-parts/staff-block-grid-layout.php');
					}else{
						include(get_template_directory().'/template-parts/staff-block-list-layout.php');
					}
				}
				?>
				<?php
				return ob_get_flush();
			} );
	}
	public function InfoCarousel(){
		Block::make( __( 'Hip Info Box Carousel Block' ))
			->add_fields( array(
				Field::make( 'separator', 'hip_info_box_block_title', __( 'Info Box','tabby' )),
				Field::make( 'complex', 'tabby_info_box_content', 'Add Info box content' )
					->add_fields( array(
						Field::make( 'text', 'info_title', 'Title' ),
						Field::make( 'textarea', 'info_description', 'Description' ),
						Field::make( 'text', 'info_link_label', 'Link Name' )->set_width( 50 ),
						Field::make( 'text', 'info_link', 'Link' )->set_width( 50 ),
						Field::make( 'text', 'info_icon', 'Icon' ),
						Field::make( 'color', 'info_bg_color', 'Info box bg color' )->set_width( 50 ),
						Field::make( 'color', 'info_text_color', 'Info box color' )->set_width( 50 )
					) ),
				Field::make( 'separator', 'hip_carousel_setting', __( 'Info Box Carousel Settings','tabby' )),
				Field::make( 'checkbox', 'info_box_carousel_dot_hide', 'Carousel Dot Hide' ),
				Field::make( 'color', 'info_box_carousel_dot_color', 'Carousel Dot Color' )->set_width( 50 ),
				Field::make( 'color', 'info_box_carousel_dot_active_color', 'Carousel Dot Active Color' )->set_width( 50 ),
				Field::make( 'separator', 'hip_carousel_nav_setting', __( '' )),
				Field::make( 'checkbox', 'info_box_carousel_nav_hide', 'Carousel Nav Hide' ),
				Field::make( 'color', 'info_box_carousel_nav_color', 'Carousel Nav Color' )->set_width( 50 ),
				Field::make( 'color', 'info_box_carousel_nav_hover_color', 'Carousel Nav Hover Color' )->set_width( 50 ),
			))
			->set_category( 'hip-blocks', __( 'Hip Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				$infoBoxContent = $fields['tabby_info_box_content'];
				if(!empty($infoBoxContent)){
					include(get_template_directory().'/template-parts/info-box-layout.php');
				}
			} );
	}
	public function videoEmbed()
	{
		Block::make( __( 'Hip Video Embed' ))
			->add_fields( array(
				Field::make( 'separator', 'hip_video_embed_title', __( 'Hip Video Embed','tabby' )),
				Field::make( 'select', 'hip_video_embed_source', __( 'Video Source' ) )
					->set_options( array(
						'url' => __('Youtube Url','tabby'),
						'video_collection' => __('Video Collection','tabby')
					)),
				Field::make( 'text', 'hip_video_embed_url', 'Video Url' )
					->set_conditional_logic( array(
						'relation' => 'AND',
						array(
							'field' => 'hip_video_embed_source',
							'value' => 'url',
							'compare' => '=',
						)
					)),
				Field::make( 'select', 'hip_video_embed_video_collection', 'Select Video' )
					->set_options([$this,'getVideoCollectionPosts'])
					->set_conditional_logic( array(
						'relation' => 'AND',
						array(
							'field' => 'hip_video_embed_source',
							'value' => 'video_collection',
							'compare' => '=',
						)
					)),
			))
			->set_category( 'hip-blocks', __( 'Hip Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				$videoUrl = $fields['hip_video_embed_url'];
				if($fields['hip_video_embed_source'] == 'url'){
					$videoUrl = $this->_prepareVideoUrl($videoUrl);
				}
				if($fields['hip_video_embed_source'] == 'video_collection'){
					$videoUrl = $this->_prepareVideoUrl(get_post_meta($fields['hip_video_embed_video_collection'],'_pvc_video_embed',true));
				}
				if(!empty($videoUrl)){
					include(get_template_directory().'/template-parts/video-embed-layout.php');
				}
			} );
	}
	public function hipPostsBlock()
	{
		Block::make( __( 'Hip Post Block' ))
			->add_fields([
				Field::make( 'html', 'hip_post_block_title', __( 'Hip post block' ) )
					->set_html('<h1>Hip post block</h1>'),
				Field::make( 'separator', 'hip_post_block_content_title', __( 'Content','tabby' )),
				Field::make( 'select', 'hip_post_block_post_type', 'Post Type' )
					->add_options([$this,'getPostTypes']),
				Field::make( 'select', 'hip_post_block_post_cats', 'Category' )
					->set_options([
						0 => __('All','tabby')
					]),
				Field::make( 'text', 'hip_post_block_count', __( 'Number of posts' ))
					->set_attribute('pattern','^[0-9][0-9]?$|^100$')
					->set_help_text( 'Leave empty for showing all'),
				Field::make( 'select', 'hip_post_block_order', __( 'Order' ))
					->set_options([
						'asc' => __('ASC','tabby'),
						'desc' => __('DESC')
					])
					->set_help_text( 'Ordering on post date'),
				Field::make( 'separator', 'hip_post_block_design_title', __( 'Design','tabby' )),
				Field::make( 'select', 'hip_post_block_layout', __( 'Layout','tabby' ) )
					->set_options( array(
						'grid' => __('Grid','tabby'),
						'list' => __('List'),
						'slider' => __('Slider'),
					)),
				Field::make( 'select', 'hip_post_block_slider_nav', __( 'Navigation' ))
					->set_conditional_logic( array(
						'relation' => 'AND',
						array(
							'field' => 'hip_post_block_layout',
							'value' => 'slider',
							'compare' => '=',
						)
					))
					->set_options([
						'none' => __('None','tabby'),
						'dot' => __('Dots','tabby'),
						'arrow' => __('Arrow','tabby'),
						'both' => __('Both','tabby'),
					]),
				Field::make( 'text', 'hip_post_block_excerpt', __( 'Excerpt Length' ))
					->set_attribute('pattern','^[0-9][0-9]?$|^100$')
					->set_help_text( 'Default value is 20 words'),
			])
			->set_category( 'hip-blocks', __( 'Hip Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				$postsPerPage = $fields['hip_post_block_count'];
				$postType = !empty($fields['hip_post_block_post_type']) ? $fields['hip_post_block_post_type']: 'post';
				$termId = !empty($fields['hip_post_block_post_cats']) ? $fields['hip_post_block_post_cats']: '';
				$excerptLength = $fields['hip_post_block_excerpt'];
				if(empty($excerptLength) && !is_numeric($excerptLength)){
					$excerptLength = 20;
				}
				$args = [];
				$args['post_type'] = $postType;
				if(empty($postsPerPage) && !is_numeric($postsPerPage)){
					$postsPerPage = 3;
				}
				$args['posts_per_page'] = $postsPerPage;
				if(!empty($termId)){
					$taxonomies = get_taxonomies( array(
						'object_type' => [$postType],
					), 'ids', 'and' );
					$taxonomy = reset($taxonomies);
					if($taxonomy != null){
						$args['tax_query']=array([
							'taxonomy' => $taxonomy->name,
							'field'    => 'term_id',
							'terms'    => [$termId],
						]);
					}
				}

				$args['order_by'] = 'id';
				$args['order'] = !empty($fields['hip_post_block_order']) ? $fields['hip_post_block_order']: 'post';
				$postBlockPosts = get_posts($args);
				if ($fields['hip_post_block_layout'] == 'slider'){
					include(get_template_directory().'/template-parts/post-block-slider-layout.php');
				}elseif($fields['hip_post_block_layout'] == 'list'){
					include(get_template_directory().'/template-parts/post-block-list-layout.php');
				}else{
					include(get_template_directory().'/template-parts/post-block-grid-layout.php');
				}
			});

	}
	public function getCategories()
	{
		$fields = [];
		$postTypes = get_post_types(['public'=>true],'objects');
		foreach ($postTypes as $postType){
			if($postType->name == 'fl-builder-template' ||$postType->name == 'attachment'){
				continue;
			}
			$taxonomies = get_taxonomies( array(
				'object_type' => [$postType->name],
			), 'ids', 'and' );
			$taxonomy = reset($taxonomies);
			if($taxonomy != null){
				$options = array();
				$terms   = get_terms( array(
					'taxonomy'   => $taxonomy->name,
					'hide_empty' => true,
				));

				if (!empty($terms)) {
					foreach( $terms as $term ) {
						$options[ $term->term_id ] = $term->name;
					}
					$termField = Field::make( 'select', "terms_{$taxonomy->name}", "{$taxonomy->label}" )
						->set_conditional_logic( array(
							array(
								'field'   => 'hip_post_block_post_type',
								'compare' => '=',
								'value'   => $postType->name,
							)
						) )
						->add_options($options);
					array_push($fields,$termField);
				}
			}
		}
		return $fields;
	}
	private function _prepareVideoUrl($videoUrl)
	{
		if (stripos($videoUrl,'youtube') || stripos($videoUrl,'youtu.be')){
			if(stripos($videoUrl,'iframe')){
				preg_match('/(?<=\/embed\/)[^"\']*/', $videoUrl, $matches);
				$video_id = $matches[0];
			}else{
				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/",$videoUrl, $matches);
				$video_id = $matches[1];
			}
			$processedUrl = 'https://www.youtube.com/embed/'.$video_id.'?enablejsapi=1&rel=0';
		}else{
			$processedUrl = $videoUrl;
		}
		return $processedUrl;

	}
}