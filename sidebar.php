<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tabby
 */

if ( ! is_active_sidebar( 'page_sidebar' ) ) {
	return;
}
?>

<aside id="page-sidebar" class="widget-area">
	<?php dynamic_sidebar( 'page_sidebar' ); ?>
</aside><!-- #secondary -->
