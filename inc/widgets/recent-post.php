<?php

class tabbyRecentPosts extends WP_Widget{
	public function __construct(){
		parent::__construct('tabbyRecentPosts','Tabby Recent posts', array(
			'description' => 'Add Tabby Theme Recent Posts'
		));
	}

	/**
	 * widget output layout
	 */
	public function widget($args,$instance){

		$title = $instance['title'];
		$numberOfPost = $instance['numberOfPost'];

		echo $args['before_widget'].''.
			$args['before_title'].''.$title.''.
			$args['after_title'];
		?>
		<?php
		$args = array(
			'posts_per_page' => $numberOfPost,
			'post_type' => 'post'
		);
		$recentPostQuery = new WP_Query($args);

		if( $recentPostQuery->have_posts() ) {
			echo '<div class="recent-posts my-4">';
			while ( $recentPostQuery->have_posts() ) : $recentPostQuery->the_post();
				?>
					<div class="recent-item d-flex align-items-center">
						<?php if(has_post_thumbnail()): ?>
						<div class="recent-post-img d-inline-block mr-3">
							<?php the_post_thumbnail('100x65', array('class'=> 'img-fluid rounded', 'alt' => 'recent-blog-img')); ?>
						</div>
						<?php endif; ?>
						<div class="content">
							<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
							<p class="time mb-0"><?php echo get_the_time('F j, Y' ); ?></p>
						</div>
					</div>
				<?php
			endwhile;
		} else {
			echo '<h5>'.__( 'No Post added yet!', 'tabby' ).'</h5>';
		}
		// Reset Query
		wp_reset_query();
		?>

		<?php

		echo '</div>'.isset($args['after_widget']);
	}

	/**
	 * widget form input
	 */
	public function form($instance){

		$title = $instance['title'];
		$numberOfPost = $instance['numberOfPost'];
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','tabby'); ?></label></p>
		<p><input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>"/></p>

		<p><label for="<?php echo $this->get_field_id('numberOfPost'); ?>">
				<?php _e('Show Numbers of Recent Posts','tabby'); ?></label></p>
		<p><input type="number" class="widefat" id="<?php echo $this->get_field_id('numberOfPost'); ?>" name="<?php echo $this->get_field_name('numberOfPost'); ?>" value="<?php echo $numberOfPost; ?>"/></p>
		<?php
	}

	/**
	 * widget fields data update
	 */
	public function update($newInstance,$oldInstance){
		$instance = $oldInstance;
		$instance['title'] = $newInstance['title'];
		$instance['numberOfPost'] = $newInstance['numberOfPost'];
		return $instance;
	}

}

/**
 * widgets init
 */
function tabbyRecentPostWidget(){
	register_widget('tabbyRecentPosts');
}
add_action('widgets_init','tabbyRecentPostWidget');