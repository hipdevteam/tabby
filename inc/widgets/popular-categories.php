<?php

class tabbyPopularCategories extends WP_Widget{
	public function __construct(){
		parent::__construct('tabbyPopularCategories','Tabby Popular Categories', array(
			'description' => 'Add Tabby Theme Popular Categories'
		));
	}

	/**
	 * widget output layout
	 */
	public function widget($args,$instance)
	{
		$title = $instance['title'];
		$numberOfCategories = $instance['numberOfCategories'];
		echo $args['before_widget'] . '' .
			 $args['before_title'] . '' . $title . '' .
			 $args['after_title'];

			 $popularCatArgs = array(
				'posts_per_page' => $numberOfCategories,
				'orderby' => 'meta_value_num',
				'order' => 'DESC',
				'post_type' => 'post'
			);

		echo '<ul class="popular-categories-items list-unstyled my-4">';
		foreach (get_categories($popularCatArgs) as $term) {
		?>
			<li>
				<a href="<?php echo get_term_link($term->term_id)?>"><?php echo $term->name; ?></a>
			</li>
			<?php
		}
		echo '</ul>' . $args['after_widget'];
	}

	/**
	 * widget form input
	 */
	public function form($instance){

		$title = $instance['title'];
		$numberOfCategories = $instance['numberOfCategories'];
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title','tabby'); ?></label></p>
		<p><input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>"/></p>

		<p><label for="<?php echo $this->get_field_id('numberOfCategories'); ?>">
				<?php _e('Show Numbers of Popular Categories','tabby'); ?></label></p>
		<p><input type="number" class="widefat" id="<?php echo $this->get_field_id('numberOfCategories'); ?>" name="<?php echo $this->get_field_name('numberOfCategories'); ?>" value="<?php echo $numberOfCategories; ?>"/></p>
		<?php
	}

	/**
	 * widget fields data update
	 */
	public function update($newInstance,$oldInstance){
		$instance = $oldInstance;
		$instance['title'] = $newInstance['title'];
		$instance['numberOfCategories'] = $newInstance['numberOfCategories'];
		return $instance;
	}

}

/**
 * widgets init
 */
function tabbyPopularCategoriesWidget(){
	register_widget('tabbyPopularCategories');
}
add_action('widgets_init','tabbyPopularCategoriesWidget');