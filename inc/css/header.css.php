<?php
	global  $tabbyFields;
	$tabbarVisibleWidth = $tabbyFields['tabby_hide_windows_larger_than'];
	$mobileMenuLayout =  $tabbyFields['tabby_menu_layout'];
	$breakpoint = empty($tabbarVisibleWidth) ? '991' : $tabbarVisibleWidth;
	$isSticky = $tabbyFields['tabby_sticky_header'];
	$headerHeightTop = (int)$tabbyFields['tabby_header_height_top'];
	$headerMainHeight = (int)$tabbyFields['tabby_header_height_main'];
?>

<?php if (!empty($isSticky) == 'yes') : ?>
	@media(min-width: <?php echo $breakpoint+1; ?>px){
		<?php $topHeaderHeight = $headerHeightTop ? $headerHeightTop : '52'; ?>
		body.tabby-sticky-header #page {
			padding-top: <?php echo $topHeaderHeight; ?>px;
			position: relative;
		}
		body.header-is-sticky #page {
			padding-top: 97px;
		}
		body.tabby-sticky-header #page header.header {
			position: fixed;
			left: 0;
			right: 0;
			top: 0;
			z-index: 999;
			background-color: #fff;
		}
		body.header-is-sticky #page header.header {
			-webkit-box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
			-moz-box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
			box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
		}
		body.tabby-sticky-header.logged-in.admin-bar #page header.header {
			top: 32px;
		}
		header.header .header-top-bar {
			height: 52px;
			overflow: hidden;
			-webkit-transition: all .3s ease;
			-moz-transition: all .3s ease;
			-o-transition: all .3s ease;
			transition: all .5s ease;
		}
		body.header-is-sticky #page header.header .header-top-bar {
				height: 0px;
		}

		<?php
		if (!empty($headerHeightTop)) :
			$percentage = ((20*$headerHeightTop)/100);
			?>
			header.header .header-top-bar{
				height: <?php echo $headerHeightTop?>px;
			}
			body.header-is-sticky #page header.header .header-top-bar {
				height: <?php echo $headerHeightTop - $percentage ;?>px;
			}
			body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
				height: <?php echo $headerHeightTop - $percentage ;?>px;
				line-height: <?php echo $headerHeightTop - $percentage ;?>px;
			}
		<?php endif; ?>


	<?php if ($tabbyFields['tabby_header_cta_btn_style'] =='oval') :?>
		<?php if (!empty($headerHeightTop)) :
			$percentage = ((40*$headerHeightTop)/100);
			?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: <?php echo $headerHeightTop - $percentage ;?>px;
			line-height: <?php echo $headerHeightTop - $percentage ;?>px;
		}
		<?php else : ?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: 34px;
			line-height: 34px;
			padding: 2px 14px;
		}
		<?php endif; ?>

	<?php else : ?>
		<?php if (!empty($headerHeightTop)) :
			$percentage = ((20*$headerHeightTop)/100);
			?>
			body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
				font-size: 15px;
				height: <?php echo $headerHeightTop - $percentage ;?>px;
				line-height: <?php echo $headerHeightTop - $percentage ;?>px;
			}
		<?php else : ?>
			body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
				font-size: 15px;
				height: 42px;
				line-height: 42px;
			}
		<?php endif; ?>

	<?php endif; ?>

	header.header .header-main {
		height: 68px;
		-webkit-transition: all .3s ease;
		-moz-transition: all .3s ease;
		-o-transition: all .3s ease;
		transition: all .3s ease;
	}
	body.header-is-sticky #page header.header .header-main {
		height: 55px;
	}
	<?php if (!empty($headerMainHeight)) : ?>
		header.header .header-main {
			height: <?php echo $headerMainHeight; ?>px;
		}
	<?php endif; ?>
	<?php
	if (!empty($headerMainHeight = $headerMainHeight ? $headerMainHeight : "68")) :
		$percentage = ((20*$headerMainHeight)/100);
		?>
		body.header-is-sticky #page header.header .header-main {
			height: <?php echo $headerMainHeight - $percentage ?>px;
		}
	<?php endif; ?>
	body.header-is-sticky #page header.header .header-main .logo-wrap a img {
		max-width: 180px;
	}
	body.header-is-sticky #page header.header .header-main ul.menu>li a {
		font-size: .9rem;
	}
	body.header-is-sticky #page header.header .header-main ul.menu>li.menu-item-has-children>ul.sub-menu {
		padding-top: 16px;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page header.header,
	body.tabby-sticky-header.tabby-header-layout-4 #page header.header{
		background: none;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page,
	body.tabby-sticky-header.tabby-header-layout-4 #page{
		padding-top: 48px;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page header.header .header-main,
	body.tabby-sticky-header.tabby-header-layout-4 #page header.header .header-main {
		position: relative;
	}
}
<?php endif; ?>

/** Header Top **/
<?php $isCtaStyle = $tabbyFields['tabby_header_cta_btn_style']; ?>
<?php if (!empty($headerHeightTop)) :?>
	<?php if ($isCtaStyle =='oval') :?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: 52px;
			line-height: 52px;
			display: flex;
			align-items: center;
		}
	<?php else : ?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: <?php echo $headerHeightTop;?>px;
			line-height: <?php echo $headerHeightTop;?>px;
			display: flex;
			align-items: center;
		}
	<?php endif; ?>
<?php else : ?>
	<?php if ($isCtaStyle =='oval') : ?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: 36px;
			line-height: 36px;
			display: flex;
			align-items: center;
			margin: 10px 0;
		}
	<?php else : ?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: 52px;
			line-height: 52px;
			display: flex;
			align-items: center;
		}
	<?php endif; ?>
<?php endif; ?>



/** Header Main **/
<?php if (!empty($headerMainHeight)) : ?>
	header.header .header-main {
	height: <?php echo $headerMainHeight; ?>px;
}
<?php endif; ?>

/** Header CTA settings **/
<?php $HeaderTopCtpTextColor = $tabbyFields['tabby_header_cta_btn_bg_text_color']; ?>
<?php $HeaderTopCtpBgColor = $tabbyFields['tabby_header_cta_btn_bg_color']; ?>
<?php $HeaderTopCtpTextHoverColor = $tabbyFields['tabby_header_cta_btn_bg_text_hover_color']; ?>
<?php $HeaderTopCtpBgHoverColor = $tabbyFields['tabby_header_cta_btn_bg_hover_color']; ?>


<?php if (!empty($HeaderTopCtpTextColor)) : ?>
	header.header ul li.header-top-btn a{
		color: <?php echo $HeaderTopCtpTextColor;?>;
	}
<?php endif; ?>
<?php if (!empty($HeaderTopCtpBgColor)) : ?>
	header.header ul li.header-top-btn a{
		background: <?php  echo $HeaderTopCtpBgColor; ?>;
	}
<?php endif; ?>
<?php if (!empty($HeaderTopCtpTextHoverColor)) : ?>
	header.header ul li.header-top-btn a:hover{
		color: <?php echo $HeaderTopCtpTextHoverColor;?>;
	}
<?php endif; ?>
<?php if (!empty($HeaderTopCtpBgHoverColor)) : ?>
	header.header ul li.header-top-btn a:hover{
		background: <?php  echo $HeaderTopCtpBgHoverColor; ?>;
	}
<?php endif; ?>

header.header ul li.header-top-btn a{
	transition: 0.5s;

	<?php if ($isCtaStyle =='rectangle') : ?>
	padding: 0 18px ;
	border-radius: 0 !important;
		<?php if ($ctaBorderBottomStyle = $tabbyFields['tabby_header_cta_btn_border_bottom'] == 'yes') :?>
			<?php if (!empty($ctaBorderBottomStyle)) :?>
					<?php $ctaBorderBottomColor = $tabbyFields['tabby_header_cta_btn_border_btn_color']; ?>
					<?php if (!empty($ctaBorderBottomWidth = $tabbyFields['tabby_header_cta_btn_border_btn_height'])) :?>
					border-bottom: <?php echo (int)$ctaBorderBottomWidth;?>px solid <?php echo $ctaBorderBottomColor;?>!important;
					<?php else :?>
					border-bottom: 2px;
					<?php endif;?>
			<?php endif;?>
		<?php endif;?>
	<?php endif;?>

	<?php if ($isCtaStyle =='oval') :?>
		<?php $borderColor = ($tabbyFields['tabby_header_cta_btn_border_outside_btn_color']) ?  $tabbyFields['tabby_header_cta_btn_border_outside_btn_color'] : "red"; ?>
		<?php if ($ctaBorderOutsideStyle = $tabbyFields['tabby_header_cta_btn_border_outside'] == 'yes') :?>
			<?php if (!empty($ctaBorderOutsideStyle)) :?>
				<?php $ctaBorderOutsideWidth = $tabbyFields['tabby_header_cta_btn_border_outside_width'] || "2px";
				?>
				border-bottom: <?php echo (int)$ctaBorderOutsideWidth . "px solid " . $borderColor; ?>!important;
				<?php if (!empty($borderColor)) :?>
					border-color: <?php echo $borderColor;?>!important;
				<?php endif;?>
					<?php if (!empty($ctaBorderOutsideWidth = $tabbyFields['tabby_header_cta_btn_border_outside_width'])) :?>
						border-width: <?php echo (int)$ctaBorderOutsideWidth;?>px !important;
					<?php endif;?>
					border-style: solid;
					border-radius: 50px;
			<?php endif;?>
		<?php else :?>
			border-radius: 50px;
			border: none;
			line-height: 20px;
		<?php endif;?>
	<?php endif;?>
	}

header.header ul li.header-top-btn a:hover{
	<?php if (!empty($ctaBtnBgHover = $tabbyFields['tabby_header_cta_btn_bg_hover_color'])) :?>
	background:<?php echo $ctaBtnBgHover;?>;
	<?php endif;?>
	<?php if (!empty($ctaBtncolorHover = $tabbyFields['tabby_header_cta_btn_bg_text_hover_color'])) :?>
	color: <?php echo $ctaBtncolorHover;?>;
	<?php endif;?>

	<?php if ($tabbyFields['tabby_header_cta_btn_style'] =='oval') :?>
		<?php if ($ctaBorderOutsideStyle = $tabbyFields['tabby_header_cta_btn_border_outside'] == 'yes') :?>
			<?php if (!empty($ctaBorderOutsideStyle)) :?>
				<?php if (!empty($ctaBorderOutsideHoverColor = $tabbyFields['tabby_header_cta_btn_border_outside_btn_hover_color'])) :?>
					border-color: <?php echo $ctaBorderOutsideHoverColor;?>;
					border-bottom-color: <?php echo $ctaBorderOutsideHoverColor;?>;
				<?php endif;?>
			<?php endif;?>
		<?php endif;?>
	<?php else : ?>
		<?php if ($ctaBorderBottomStyle = $tabbyFields['tabby_header_cta_btn_border_bottom'] == 'yes') :?>
			<?php if (!empty($ctaBorderBottomStyle)) :?>
					<?php if (!empty($ctaBorderBottomHover = $tabbyFields['tabby_header_cta_btn_border_btn_hover_color'])) :?>
						border-bottom-color: <?php echo $ctaBorderBottomHover;?>;
					<?php endif;?>
			<?php endif;?>
		<?php endif;?>
	<?php endif;?>
}

/** Page Banner Height Setting **/
<?php $PageBannerHeight = (int)$tabbyFields['tabby_header_height_page_header']; ?>
<?php if (!empty($PageBannerHeight)) :?>
	.banner{
		min-height: <?php echo $PageBannerHeight;?>px;
		<?php if (!empty($isSticky)) : ?>
		margin-top: <?php echo $headerHeightTop +$headerMainHeight - 120; ?>px;
		<?php endif; ?>
	}
<?php endif; ?>


<?php if (!empty($mainNavBgColor = $tabbyFields['tabby_header_main_desktop_bg'])) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li{
	background-color: <?php echo $mainNavBgColor;?>;
}
<?php endif; ?>
<?php if (!empty($mainNavBgHover= $tabbyFields['tabby_header_main_desktop_bg_hover']) && !empty($mainNavTextHover = $tabbyFields['tabby_header_main_desktop_text_hover'])) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li a:hover,
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li ul.sub-menu li a:hover{
	background-color: <?php echo $mainNavBgHover;?>;
	color: <?php echo $mainNavTextHover;?>;
}
<?php endif; ?>

<?php if (!empty($mainNavTextColor = $tabbyFields['tabby_header_main_desktop_text_color'])) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li a{
	color: <?php echo $mainNavTextColor;?>;
}
<?php endif; ?>

<?php
$topHeaderHeight = (int)$tabbyFields['tabby_header_height_top'] ? (int)$tabbyFields['tabby_header_height_top'] : '52';
$mainHeaderHeight = (int)$tabbyFields['tabby_header_height_main'] ? (int)$tabbyFields['tabby_header_height_main'] : "68";
?>

<?php if (is_front_page()  && (!empty($tabbyFields['tabby_header_height_main']) || !empty((int)$tabbyFields['tabby_header_height_top']))) :
	?>
	.hero-wrapper.ugb-container.ugb-container--height-full {
		min-height: calc(100vh - <?php echo $mainHeaderHeight + $topHeaderHeight .'px'; ?>);
	}
	<?php if (!empty($isSticky) == 'yes') : ?>
		@media(min-width: 992px ){
			.home .site .site-content#content{
				margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
			}
		}
	<?php endif; ?>
<?php elseif (is_front_page()) : ?>
	.hero-wrapper.ugb-container.ugb-container--height-full {
		min-height: calc(100vh - <?php echo $mainHeaderHeight + $topHeaderHeight .'px'; ?>);
	}
	<?php if (!empty($isSticky) == 'yes') : ?>
		@media(min-width: 992px ){
			.home .site .site-content#content{
				margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
			}
		}
	<?php endif; ?>
<?php else : ?>
	<?php if (!empty($isSticky) == 'yes') : ?>
		@media(min-width: 992px ){
			.banner{
				margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
			}
		}
	<?php endif; ?>
<?php endif; ?>

/** Mobile header main **/
@media(max-width: 991px) {
	<?php if (!empty($headerManiMobileHeight = $tabbyFields['tabby_header_height_main_mobile'])) : ?>
		header.header .header-main{
			height:<?php echo (int)$headerManiMobileHeight; ?>px;
			padding: 0;
		}
	<?php endif;?>
	<?php if (!empty($headerBannerMobileHeight = $tabbyFields['tabby_header_height_page_header_mobile'])) : ?>
		.banner{
			min-height:<?php echo (int)$headerBannerMobileHeight; ?>px;
		}
	<?php endif;?>
}

/** Mobile Header Top bar **/
@media(max-width: <?php echo $breakpoint; ?>px){
	<?php if (!empty($mobileMenuLayout) == 'tabbar_top') : ?>
		body.tabby-sticky-header.tabby-tabbar-top header.header{
			display: none !important;
		}
		body.tabby-sticky-header.tabby-tabbar-top #page{
			padding-top: 0;
		}
	<?php endif;?>
}

<?php if (!empty($subMenuIconColor = $tabbyFields['tabby_header_main_desktop_icon_color'])):?>
header.header .header-main ul.menu>li.menu-item-has-children>ul.sub-menu>li.menu-item-has-children:after{
	color: <?php echo $subMenuIconColor;?>;
}
<?php endif;?>
<?php if (!empty($headerTopBg = $tabbyFields['tabby_header_top_bg'])):?>
header.header .header-top-bar{
	background: <?php echo $headerTopBg;?>
}
<?php endif;?>
<?php if (!empty($headerTopLinkColor = $tabbyFields['tabby_header_top_link_color'])):?>
header.header .header-top-info a,
header.header .tabby-top-menu ul li a{
	color: <?php echo $headerTopLinkColor;?>
}
<?php endif;?>
<?php if (!empty($headerTopLinkColorHover = $tabbyFields['tabby_header_top_link_hover'])):?>
header.header .header-top-info a:hover,
header.header .tabby-top-menu ul li a:hover{
	color: <?php echo $headerTopLinkColorHover;?>
}
<?php endif;?>
<?php if (!empty($mainMenuLiColor = $tabbyFields['tabby_header_menu_color'])):?>
header.header .header-main ul.menu li a{
	color: <?php echo $mainMenuLiColor;?>
}
<?php endif;?>
<?php if (!empty($mainMenuLiColorHover = $tabbyFields['tabby_header_menu_color_hover'])):?>
header.header .header-main ul.menu li a:hover{
	color: <?php echo $mainMenuLiColorHover;?>
}
<?php endif;?>
