<?php
  global  $tabbyFields;
?>
<?php if (!empty($footerPreSectionHeight = $tabbyFields['tabby_above_footer_section_height'])) : ?>
footer.site-footer .top-footer{
	min-height: <?php echo $footerPreSectionHeight;?>px;
}
<?php endif; ?>

<?php if (!empty($footerPreEmailBg = $tabbyFields['tabby_above_footer_email_bg'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-field input[type=email]{
	background: <?php echo $footerPreEmailBg;?>;
}
<?php endif; ?>
<?php if (!empty($footerPreEmailColor = $tabbyFields['tabby_above_footer_email_text'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-field input[type=email]{
	color: <?php echo $footerPreEmailColor;?>;
}
<?php endif; ?>
<?php if (!empty($footerPreEmailPlaceholder = $tabbyFields['tabby_above_footer_email_placeholder'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-field input[type=email]::placeholder{
	color: <?php echo $footerPreEmailPlaceholder;?>;
}
<?php endif; ?>

<?php if(!empty($mainFooterBg = $tabbyFields['tabby_main_footer_bg_color'])):?>
footer.site-footer .main-footer{
	background-color: <?php echo $mainFooterBg; ?>;
}
<?php endif;?>

<?php if(!empty($subFooterBg = $tabbyFields['tabby_sub_footer_bg_color'])):?>
footer.site-footer .sub-footer{
	background-color: <?php echo $subFooterBg; ?>;
}
footer.site-footer .sub-footer .container{
	border: 0;
}
<?php endif;?>
<?php
$topFooterBgImage = $tabbyFields['tabby_above_footer_bg'];
$topFooterBgImageOpacity = $tabbyFields['tabby_above_footer_bg_opacity'];
$topFooterBgColor = $tabbyFields['tabby_above_footer_bg_color'];
if(!empty($topFooterBgImage)):
?>
footer.site-footer .top-footer:before{
	content: '';
	position: absolute;
	left: 0;
	right: 0;
	top:0;
	bottom: 0;
	background-image: url("<?php echo wp_get_attachment_image_src($topFooterBgImage,'full')[0];?>");
	opacity: <?php echo !empty($topFooterBgImageOpacity) ? $topFooterBgImageOpacity / 100 :'1'?>;
	background-size: cover;
	background-position: center;
	z-index: 1;
}
footer.site-footer .top-footer .container{
	position: relative;
	z-index: 2;
}
<?php endif;?>
<?php if(!empty($topFooterBgColor)):?>
div#page footer.site-footer .top-footer{
	background-color: <?php echo $topFooterBgColor;?>;
}
<?php endif;?>

/* above footer settings*/

<?php if (!empty($footerPreBtnBg = $tabbyFields['tabby_above_footer_btn_bg']) ) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]{
	background: <?php echo $footerPreBtnBg;?>;
	transition: all 0.5s ease-in-out;
}
<?php endif; ?>
<?php if (!empty($footerPreBtnTextColor = $tabbyFields['tabby_above_footer_btn_text_color'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]{
	color: <?php echo $footerPreBtnTextColor;?>;
}
<?php endif; ?>
<?php if (!empty($footerPreBtnBgHover = $tabbyFields['tabby_above_footer_btn_hover']) ) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover{
	background: <?php echo $footerPreBtnBgHover;?>;
}
<?php endif; ?>
<?php if (!empty($footerPreBtnTextHover = $tabbyFields['tabby_above_footer_btn_text_hover'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover{
	color: <?php echo $footerPreBtnTextHover;?>;
}
<?php endif; ?>
<?php if (!empty($footerPreSectionHeight = $tabbyFields['tabby_above_footer_section_height'])) : ?>
footer.site-footer .top-footer{
	min-height: <?php echo $footerPreSectionHeight;?>px;
}
<?php endif; ?>

/*
 * footer Main
 */
<?php if (!empty($mainFooterHeadingColor = $tabbyFields['tabby_main_footer_heading_color'])) : ?>
footer.site-footer .main-footer .widget .widget-title{
	color: <?php echo $mainFooterHeadingColor;?>;
}
<?php endif; ?>
<?php if (!empty($mainFooterTextColor = $tabbyFields['tabby_main_footer_text_color'])) : ?>
footer.site-footer .main-footer p{
	color: <?php echo $mainFooterTextColor;?>;
}
<?php endif; ?>
<?php if (!empty($mainFooterLinkColor = $tabbyFields['tabby_main_footer_link_color'])) : ?>
footer.site-footer ul.menu li a, footer.site-footer p a{
	color: <?php echo $mainFooterLinkColor;?>;
}
<?php endif; ?>
<?php if (!empty($mainFooterLinkHoverColor = $tabbyFields['tabby_main_footer_link_hover_color'])) : ?>
footer.site-footer ul.menu li a:hover, footer.site-footer p a:hover{
	color: <?php echo $mainFooterLinkHoverColor;?>;
}
<?php endif; ?>

/*
 * footer bottom section
 */
<?php if (!empty($subFooterTextColor = $tabbyFields['tabby_sub_footer_text_color'])) : ?>
footer.site-footer .sub-footer p{
	color: <?php echo $subFooterTextColor;?>;
}
<?php endif; ?>
<?php if (!empty($subFooterLinkColor = $tabbyFields['tabby_sub_footer_link_color'])) : ?>
footer.site-footer .sub-footer a{
	color: <?php echo $subFooterLinkColor;?>;
}
<?php endif; ?>
<?php if (!empty($subFooterLinkHoverColor = $tabbyFields['tabby_sub_footer_link_hover_color'])) : ?>
footer.site-footer .sub-footer a:hover{
	color: <?php echo $subFooterLinkHoverColor;?>;
}
<?php endif; ?>

<?php if (!empty($subFooterSubmitErrorMessageColor = $tabbyFields['tabby_above_footer_submit_error_message_color'])) : ?>
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-error{
	color: <?php echo $subFooterSubmitErrorMessageColor;?>;
}
<?php endif; ?>

footer.site-footer .wpforms-confirmation-container-full{
<?php if (!empty($subFooterSubmitSuccessMessageColor = $tabbyFields['tabby_above_footer_submit_success_message_bg'])) : ?>
	background: <?php echo $subFooterSubmitSuccessMessageColor; ?>;
<?php endif; ?>
<?php if (!empty($subFooterSubmitSuccessBorderColor = $tabbyFields['tabby_above_footer_submit_success_message_border_color'])) : ?>
	border: 1px solid <?php echo $subFooterSubmitSuccessBorderColor; ?>;
<?php endif; ?>
<?php if (!empty($subFooterSubmitSuccessMessageColor = $tabbyFields['tabby_above_footer_submit_success_message_color'])) : ?>
	color: <?php echo $subFooterSubmitSuccessMessageColor; ?>;
<?php endif; ?>
}
footer.site-footer .wpforms-confirmation-container-full p{
<?php if (!empty($subFooterSubmitSuccessMessageColor = $tabbyFields['tabby_above_footer_submit_success_message_color'])) : ?>
	color: <?php echo $subFooterSubmitSuccessMessageColor; ?>;
<?php endif; ?>
}