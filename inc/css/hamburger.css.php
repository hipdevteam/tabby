<?php
	global $tabbyFields;
	$hamburgerWrapperWidth = $tabbyFields['tabby_hamburger_menu_width'];
	if($hamburgerWrapperWidth =='container-width'):
	$hamburgerWrapperBg = $tabbyFields['tabby_hamburger_wrapper_bg'];
	if(!empty($hamburgerWrapperBg)):
?>
.mobile-menu-wrap.hamburger-menu-wrap{
	background: <?php echo $hamburgerWrapperBg; ?>;
}
<?php endif; endif; ?>

<?php
	$hamburgerIconColor = $tabbyFields['tabby_hamburger_icon_color'];
	if(!empty($hamburgerIconColor)):
?>
.hamburger-btn-wrap button span:before, .hamburger-btn-wrap button span:after, .hamburger-btn-wrap button span{
	background: <?php echo $hamburgerIconColor; ?>;
}
<?php endif; ?>
<?php
	$hamburgerMenuColor = $tabbyFields['tabby_hamburger_color'];
	if(!empty($hamburgerMenuColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li a{
	color: <?php echo $hamburgerMenuColor; ?>;
}
<?php endif; ?>
<?php
	$hamburgerMenuHoverColor = $tabbyFields['tabby_hamburger_hover_color'];
	if(!empty($hamburgerMenuHoverColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li:hover a{
	color: <?php echo $hamburgerMenuHoverColor; ?>;
}
<?php endif; ?>
<?php
	$hamburgerMenuIconColor = $tabbyFields['tabby_hamburger_menu_icon_color'];
	if(!empty($hamburgerMenuIconColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap .main-menu-wrapper ul.menu li.menu-item-has-children .dropdown-btn span{
	color: <?php echo $hamburgerMenuIconColor; ?>;
}
<?php endif; ?>

<?php
	$hamburgerMenuFontSize = (int)$tabbyFields['tabby_hamburger_font_size'];
	if(!empty($hamburgerMenuFontSize)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li a{
	font-size: <?php echo $hamburgerMenuFontSize; ?>px;
}
<?php endif; ?>

<?php
	$hamburgerMenuSubmenuFontSize = (int)$tabbyFields['tabby_hamburger_submenu_font_size'];
	if(!empty($hamburgerMenuSubmenuFontSize)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li.menu-item-has-children ul.sub-menu li a{
	font-size: <?php echo $hamburgerMenuSubmenuFontSize; ?>px;
}
<?php endif; ?>
<?php
	$hamburgerMenuSubmenuColor = $tabbyFields['tabby_hamburger_menu_submenu_color'];
	if(!empty($hamburgerMenuSubmenuColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li.menu-item-has-children ul.sub-menu li a{
	color: <?php echo $hamburgerMenuSubmenuColor; ?>;
}
<?php endif; ?>
<?php
	$hamburgerMenuSubmenuHoverColor = $tabbyFields['tabby_hamburger_menu_submenu_hover_color'];
	if(!empty($hamburgerMenuSubmenuHoverColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li.menu-item-has-children ul.sub-menu li:hover a{
	color: <?php echo $hamburgerMenuSubmenuHoverColor; ?>;
}
<?php endif; ?>

<?php
	$hamburgerMenuBorderColor = $tabbyFields['tabby_hamburger_menu_border_color'];
	if(!empty($hamburgerMenuBorderColor)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li+li{
	border-color: <?php echo $hamburgerMenuBorderColor; ?>;
}
<?php endif; ?>
<?php
	$hamburgerMenuBorderHeight = (int)$tabbyFields['tabby_hamburger_menu_border_height'];
	if(!empty($hamburgerMenuBorderHeight)):
?>
.mobile-menu-wrap.hamburger-menu-wrap ul.menu li+li{
	border-width: <?php echo $hamburgerMenuBorderHeight; ?>px;
}
<?php endif; ?>

<?php
	$hamburgerWrapperWidth = $tabbyFields['tabby_hamburger_menu_width'];
	if($hamburgerWrapperWidth =='full-width'):
?>
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap .container {
		max-width: 100%;
		padding: 0;
	}
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu{
		margin: 0 auto;
	}
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li {
		padding: 0;
	}
	<?php $hamburgerWrapperBg = $tabbyFields['tabby_hamburger_menu_item_bg']; ?>
	<?php if(!empty($hamburgerWrapperBg)): ?>
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li {
		background: <?php echo $hamburgerWrapperBg; ?>;
	}
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li a {
		padding: 10px 25px;
	}
	<?php endif; ?>
	<?php $hamburgerWrapperBgHover = $tabbyFields['tabby_hamburger_menu_item_bg_hover']; ?>
	<?php if(!empty($hamburgerWrapperBgHover)): ?>
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li:hover, .hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li.rotated{
		background: <?php echo $hamburgerWrapperBgHover; ?>;
	}
	<?php endif; ?>
	.main-menu-wrapper ul.menu li.menu-item-has-children .dropdown-btn {
		right: 0;
		top: 0;
		width: 30%;
		padding-right: 25px;
		padding-top: 8px;
	}
	<?php $hamburgerBorderColor = $tabbyFields['tabby_hamburger_menu_border_color']; ?>
	<?php $hamburgerBorderHeight = (int)$tabbyFields['tabby_hamburger_menu_border_height']; ?>
	<?php $hamburgerBorderHeight = $hamburgerBorderHeight ? $hamburgerBorderHeight : '1' ?>
	<?php if(!empty($hamburgerBorderColor) || !empty($hamburgerBorderHeight)): ?>
	.mobile-menu-wrap.hamburger-menu-wrap ul.menu li.menu-item-has-children ul.sub-menu {
		margin: 0;
		border-top: <?php echo $hamburgerBorderHeight?>px solid <?php echo $hamburgerBorderColor; ?>;
	}
	<?php  endif; ?>
	<?php $hamburgerSubmenuItemBg = $tabbyFields['tabby_hamburger_submenu_item_bg']; ?>
	<?php if(!empty($hamburgerSubmenuItemBg)): ?>
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li ul.sub-menu li{
		background: <?php echo $hamburgerSubmenuItemBg; ?>;
	}
	<?php endif; ?>

	<?php $hamburgerSubmenuItemBgHover = $tabbyFields['tabby_hamburger_submenu_item_bg_hover']; ?>
	<?php if(!empty($hamburgerSubmenuItemBgHover)): ?>
	.hamburger-menu-wrap-continer .mobile-menu-wrap.hamburger-menu-wrap ul.menu li ul.sub-menu li:hover{
		background: <?php echo $hamburgerSubmenuItemBgHover; ?>;
	}
	<?php endif; ?>
	<?php $hamburgerMenuWrapperBg = $tabbyFields['tabby_hamburger_wrapper_bg_full_width']; ?>
	<?php if(!empty($hamburgerMenuWrapperBg)): ?>
		.mobile-menu-wrap.hamburger-menu-wrap{
			background: <?php echo $hamburgerMenuWrapperBg; ?>;
		}
	<?php endif; ?>
<?php endif; ?>
