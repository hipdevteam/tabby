<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php while ( have_posts() ): the_post(); ?>
			<div class="single-staff-wrapper my-5">
				<div class="container">
					<div class="row">
						<?php if(has_post_thumbnail()): ?>
							<div class="col-12 col-md-4">
								<div class="img-wrapper text-center text-md-left mb-4 mb-md-0">
									<?php the_post_thumbnail('medium', array('class'=>'img-fluid rounded', 'alt'=>'staff-image')); ?>
								</div>
							</div>
							<div class="col-12 col-md-8">
								<div class="content-wrapper text-center text-md-left">
									<h3 class="mb-3"><?php the_title(); ?></h3>
									<div class="content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						<?php else: ?>
							<div class="col">
								<div class="content-wrapper">
									<h3 class="mb-3"><?php the_title(); ?></h3>
									<div class="content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endwhile; wp_reset_postdata(); ?>
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
